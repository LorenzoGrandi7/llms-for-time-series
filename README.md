# Forecasting Time Series with LLMs
<img src="images/image.png">

## Project Goal
The goal of the project is to forecast the next time-steps of a multivariate series using a Large Language Model

## Description
Large Language Models have proven to be extremely powerful in Natural Language Processing. 
Thanks to their transformer-based architecture and its mechanism of self-attention, they are able to effectively read,
process and write text. But recently, their capabilities have been extended also outside the NLP field, 
using them in completely unrelated tasks. In this project, you will use a reduced version of LLaMa2, a famous
open-source large language model by Meta AI, to make forecasting of a multivariate time-series. 

## Learning outcomes
After the end of this project, wou will have a proper understanding of the following concepts:
### Python
<ul>
<li>Basic syntax and fundamental statements</li>
<li>Set up of a project: packages, libraries and virtual environments</li>
<li>Understand data structures in Python, from lists, tuples, dictionaries to ndarrays, dataframes and tensors</li>
<li>OOP in Python: how classes and inheritance work</li>
<li>PyTorch to design, train and test deep learning algorithms</li>
<li>How to load and use Hugging Face models</li>
</ul>

### Large Language Models
<ul>
<li>Behind LLMs: the concept of transformer</li>
<li>Tokenization and embeddings</li>
<li>What is Zero-Shot Learning and why LLMs work so well</li>
<li>How to define a pipeline to interrogate LLMs</li>
<li>Apply LLMs to time series</li>
</ul>


## Useful material
### Data
[M100 ExaData](study_material/data/m100.pdf) - you will work on telemetry data collected from the Marconi supercomputer.
The structure and the characteristics of the datasets are explained in the paper

### Background
[Attention Is All You Need](study_material/llm-background/vaswani_2017.pdf) - the fundamental work for exploring the
transformer-based architectures, on which LLMs are built \
[LLMs are Zero-Shot Reasoners](study_material/llm-background/zero-shot.pdf) - some experiments on applying LLMs on different tasks \
[LLaMa2](study_material/llm-background/llama2.pdf) - presenting LLaMa2, an open-source LLM by Meta

### LLMs and Time Series
[Large Models for Time Series](study_material/llm-time_series/survey.pdf) - a survey providing the current state of the art of the topic \
[Time-LLM](study_material/llm-time_series/time-llm.pdf) - a model for time-series forecasting through LLMs

## Contacts
Tutor: Giovanni Esposito \
[giovanni.esposito20@unibo.it](mailto:giovanni.esposito20@unibo.it)