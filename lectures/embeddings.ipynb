{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "## Word Embeddings"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "b7cb526bdac1a06"
  },
  {
   "cell_type": "markdown",
   "source": [
    "Word embeddings are dense vector representations of words in a continuous vector space. Each word is mapped to a high-dimensional vector such that similar words are represented by vectors that are close together in the vector space. These embeddings capture semantic relationships between words based on their contextual usage in a corpus of text."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "44c00af0cde59bd"
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "initial_id",
   "metadata": {
    "collapsed": true,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:46:02.348958923Z",
     "start_time": "2024-04-10T09:46:00.705583483Z"
    }
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "\n",
    "class WordEmbeddings(nn.Module):\n",
    "\n",
    "    def __init__(self, embedding_dim, vocab_size, dropout=0.1):\n",
    "        super(WordEmbeddings, self).__init__()\n",
    "        self.embedding_dim = embedding_dim\n",
    "        self.vocab_size = vocab_size\n",
    "\n",
    "        self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim)\n",
    "        self.dropout = nn.Dropout(dropout)\n",
    "\n",
    "    def forward(self, input_ids):\n",
    "        out = self.embedding(input_ids)\n",
    "        out = self.dropout(out)\n",
    "\n",
    "        return out\n"
   ]
  },
  {
   "cell_type": "markdown",
   "source": [
    "Suppose we have a input ids vector of [24,65,12,38,94,153]. To compute a 64-dimensional embeddings:\n"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "b1ebdd8fa5936219"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "torch.Size([6, 64])"
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "input_ids = torch.IntTensor([24,65,12,38,94,153])\n",
    "vocab_size = 1000\n",
    "embedding_dim = 64\n",
    "\n",
    "embedding_layer = WordEmbeddings(embedding_dim, vocab_size)\n",
    "word_embeddings = embedding_layer(input_ids)\n",
    "word_embeddings.size()"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:56:27.962375823Z",
     "start_time": "2024-04-10T09:56:27.921673Z"
    }
   },
   "id": "fdb1d15960b8a057",
   "execution_count": 18
  },
  {
   "cell_type": "markdown",
   "source": [
    "Our embeddings for [65] will be"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "a9f65a559340323e"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([ 0.3641, -0.8075, -0.2695, -1.3103, -1.5512,  1.0835, -1.5429,  0.1329,\n",
      "        -0.8142,  0.7956, -0.3745,  1.4000, -0.7687, -0.6140,  1.3226,  0.6663,\n",
      "         1.0621, -0.0575, -0.0360, -1.0126,  0.9871, -0.0000,  0.5517,  0.4605,\n",
      "         0.1718, -0.1224,  0.1257,  1.1409, -0.2053,  0.1525,  1.1878,  1.4450,\n",
      "        -1.3639, -0.0000,  2.1162, -0.9090,  0.0000,  0.9702,  0.1345, -0.7192,\n",
      "         1.4238,  1.5231, -0.2408, -2.6702, -0.8624,  0.0413, -1.4829, -0.0000,\n",
      "         0.0418, -0.0000,  0.5398, -1.2316, -1.1088,  2.9177,  1.0394, -0.3559,\n",
      "         0.2090,  0.4438,  0.8981, -1.6745, -0.7735,  0.5384,  0.7967,  0.6737],\n",
      "       grad_fn=<SelectBackward0>)\n"
     ]
    }
   ],
   "source": [
    "print(word_embeddings[1])"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:56:33.811859607Z",
     "start_time": "2024-04-10T09:56:33.803260065Z"
    }
   },
   "id": "8a1cd17846259ecf",
   "execution_count": 19
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Positional Embeddings"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "d2a5bb5fbd8e8f52"
  },
  {
   "cell_type": "markdown",
   "source": [
    "Positional embeddings are used to encode the position or order of elements in a sequence, such as words in a sentence or tokens in a sequence of data. These embeddings provide crucial information about the sequential nature of the data, enabling models to understand the order of elements in a sequence.\n",
    "In many tasks, positional embeddings are combined with word embeddings to incorporate positional information into the representation of words in a sequence. This integration allows the model to differentiate between words based not only on their meaning but also on their position in the sequence.\n",
    "\n",
    "There are various methods for generating positional embeddings. One common approach involves using sine and cosine functions with different frequencies to generate positional encodings.\n",
    "Mathematically speaking, positional embeddings are defined as\n",
    "$$\n",
    "\\text{PE}(\\text{pos},2i) = \\sin\\left(\\frac{10000 \\cdot 2i}{d_{\\text{model}}}\\right)\n",
    "$$\n",
    "$$\n",
    "\\text{PE}(\\text{pos},2i+1) = \\cos\\left(\\frac{10000 \\cdot (2i+1)}{d_{\\text{model}}}\\right)\n",
    "$$\n",
    "\n",
    "where $pos$ is the position of the element of the sequence, $i$ is the index of the embedding vector, $d_model$ is the dimensionality of the embeddings and $1000$ is a common scaling factor"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "3f102d6b9a8b65d4"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "class PositionalEncoding(nn.Module):\n",
    "    def __init__(self, max_len, d_model):\n",
    "        super(PositionalEncoding, self).__init__()\n",
    "        self.d_model = d_model\n",
    "        \n",
    "        # Compute the positional encodings\n",
    "        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)\n",
    "        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-torch.log(torch.tensor(10000.0)) / d_model))\n",
    "        self.positional_encodings = torch.zeros(max_len, d_model)\n",
    "        self.positional_encodings[:, 0::2] = torch.sin(position * div_term)\n",
    "        self.positional_encodings[:, 1::2] = torch.cos(position * div_term)\n",
    "        \n",
    "    def forward(self, input_ids):\n",
    "        # Retrieve positional encodings for given input IDs\n",
    "        # input_ids: tensor of shape (batch_size, seq_length)\n",
    "        batch_size, seq_length = input_ids.size()\n",
    "        positional_encodings = self.positional_encodings[:seq_length, :].unsqueeze(0).expand(batch_size, -1, -1)\n",
    "        return positional_encodings"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:46:03.171566233Z",
     "start_time": "2024-04-10T09:46:03.166607748Z"
    }
   },
   "id": "8abaeb68a400dc2",
   "execution_count": 3
  },
  {
   "cell_type": "markdown",
   "source": [
    "Consider our previous input ids vector of [24,65,12,38,94,153], for which we have a 64-dimensional embedding. Positional embeddings would be computed as:"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "856d848ba2ffff5f"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "torch.Size([1, 6, 64])"
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "max_len = 6 \n",
    "d_model = 64  \n",
    "position_encoder = PositionalEncoding(max_len, d_model)\n",
    "input_ids = torch.tensor([[24, 65, 12, 38, 94, 153]])\n",
    "positional_embeddings = position_encoder(input_ids)\n",
    "positional_embeddings.size()"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:47:05.047524877Z",
     "start_time": "2024-04-10T09:47:05.004027072Z"
    }
   },
   "id": "8b226fd01f33b680",
   "execution_count": 6
  },
  {
   "cell_type": "markdown",
   "source": [
    "For example, positional embeddings of [65] will be:"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "c22b744dcf8fb5c0"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([8.4147e-01, 5.4030e-01, 6.8156e-01, 7.3176e-01, 5.3317e-01, 8.4601e-01,\n",
      "        4.0931e-01, 9.1240e-01, 3.1098e-01, 9.5042e-01, 2.3492e-01, 9.7201e-01,\n",
      "        1.7689e-01, 9.8423e-01, 1.3296e-01, 9.9112e-01, 9.9833e-02, 9.9500e-01,\n",
      "        7.4919e-02, 9.9719e-01, 5.6204e-02, 9.9842e-01, 4.2157e-02, 9.9911e-01,\n",
      "        3.1618e-02, 9.9950e-01, 2.3712e-02, 9.9972e-01, 1.7782e-02, 9.9984e-01,\n",
      "        1.3335e-02, 9.9991e-01, 9.9998e-03, 9.9995e-01, 7.4989e-03, 9.9997e-01,\n",
      "        5.6234e-03, 9.9998e-01, 4.2170e-03, 9.9999e-01, 3.1623e-03, 9.9999e-01,\n",
      "        2.3714e-03, 1.0000e+00, 1.7783e-03, 1.0000e+00, 1.3335e-03, 1.0000e+00,\n",
      "        1.0000e-03, 1.0000e+00, 7.4989e-04, 1.0000e+00, 5.6234e-04, 1.0000e+00,\n",
      "        4.2170e-04, 1.0000e+00, 3.1623e-04, 1.0000e+00, 2.3714e-04, 1.0000e+00,\n",
      "        1.7783e-04, 1.0000e+00, 1.3335e-04, 1.0000e+00])\n"
     ]
    }
   ],
   "source": [
    "positional_embeddings = positional_embeddings.squeeze(0)\n",
    "print(positional_embeddings[1])"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:57:25.742563555Z",
     "start_time": "2024-04-10T09:57:25.696763290Z"
    }
   },
   "id": "585ff9c804c06c86",
   "execution_count": 20
  },
  {
   "cell_type": "markdown",
   "source": [
    "One common approach to integrating positional embeddings with word embeddings is to simply add the positional encodings to the corresponding word embeddings. This is typically done element-wise."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "b3efcb09730907b2"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "tensor([ 1.2055, -0.2672,  0.4121, -0.5786, -1.0181,  1.9295, -1.1335,  1.0453,\n",
      "        -0.5032,  1.7461, -0.1396,  2.3720, -0.5918,  0.3702,  1.4556,  1.6574,\n",
      "         1.1620,  0.9375,  0.0389, -0.0154,  1.0433,  0.9984,  0.5938,  1.4596,\n",
      "         0.2034,  0.8771,  0.1495,  2.1406, -0.1875,  1.1523,  1.2011,  2.4450,\n",
      "        -1.3539,  0.9999,  2.1237,  0.0910,  0.0056,  1.9702,  0.1387,  0.2808,\n",
      "         1.4270,  2.5231, -0.2384, -1.6702, -0.8606,  1.0413, -1.4815,  1.0000,\n",
      "         0.0428,  1.0000,  0.5405, -0.2316, -1.1083,  3.9177,  1.0399,  0.6441,\n",
      "         0.2093,  1.4438,  0.8983, -0.6745, -0.7733,  1.5384,  0.7969,  1.6737],\n",
      "       grad_fn=<SelectBackward0>)\n"
     ]
    }
   ],
   "source": [
    "embeddings = word_embeddings + positional_embeddings\n",
    "print(embeddings[1])"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-10T09:59:02.291125534Z",
     "start_time": "2024-04-10T09:59:02.284964799Z"
    }
   },
   "id": "d68b0452a76316da",
   "execution_count": 21
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
