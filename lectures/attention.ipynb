{
 "cells": [
  {
   "cell_type": "raw",
   "source": [],
   "metadata": {
    "collapsed": false
   },
   "id": "b4302bdb04d3382d"
  },
  {
   "cell_type": "markdown",
   "source": [
    "# Self Attention in LLMs"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "b5595c6d437ab905"
  },
  {
   "cell_type": "markdown",
   "source": [
    "## What is Attention\n",
    "\n",
    "\n",
    "Attention, in the context of natural language processing (NLP) and machine learning, can be thought of as a mechanism that enables models to focus on specific parts of input data while performing a task. It mimics the selective focus mechanism observed in human cognition, where certain parts of information are prioritized over others based on relevance.\n",
    "\n",
    "In NLP tasks such as language translation, text summarization, and sentiment analysis, understanding the context and relationships between words or tokens is crucial for accurate predictions. Attention mechanisms allow models to dynamically weigh the importance of different tokens in the input sequence, enabling them to generate more contextually relevant outputs.\n",
    "\n",
    "Consider the task of translating a sentence from one language to another. Instead of treating all words equally, attention allows the model to assign higher weights to words that are more relevant for translation. This selective focus mechanism helps the model generate more accurate translations by considering the context of the input sentence.\n",
    "\n",
    "Just as in language sentences, also time series have important and non-important values. Consider, for example, a financial time series. There could be some points or groups of points that can have a bigger impact on the future with respect to others. Our goal is to leverage on the mechanism of attention to catch these patterns."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "eb8a778bf409c5a"
  },
  {
   "cell_type": "markdown",
   "source": [
    "## How it is computed\n",
    "\n",
    "The computation of attention involves three fundamental components: **queries**, **keys** and **values**. Attention is defined as\n",
    "$$\n",
    "attention(Q,K,V) = softmax(\\frac{Q*K^T}{\\sqrt{d_k}})*V\n",
    "$$\n",
    "where $Q$, $K$ and $V$ are the query, key and value matrices, and $\\sqrt{d_k}$ is a scaling factor. In simple implementation of self-attention, $Q$, $K$ and $V$ are derived from linear transformations of the input embeddings\n",
    "\n",
    "For example, suppose we want to compute attentions for an input sequence embedded as:\n",
    "\n",
    "[0.2, 0.4, -0.1]\n",
    "[0.1, 0.3, 0.5]\n",
    "[0.6, -0.2, 0.3]\n",
    "[-0.3, 0.2, 0.4]\n",
    "[0.4, 0.1, 0.3]\n",
    "[0.2, 0.5, -0.4]"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "bfb74edb94aff6c5"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "\n",
    "input_embeds = torch.Tensor([[\n",
    "    [.2, .4, -.1],\n",
    "    [.1, .3, .5],\n",
    "    [.6, -.2, .3],\n",
    "    [-.3, .2, .4],\n",
    "    [.4, .1, .3],\n",
    "    [.2, .5, -.4]\n",
    "]])\n",
    "query = input_embeds\n",
    "key = input_embeds\n",
    "value = input_embeds"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:40:00.211555615Z",
     "start_time": "2024-04-09T15:40:00.207892620Z"
    }
   },
   "id": "dd57dfc798510023",
   "execution_count": 114
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 1: multiply the queries for the keys\n",
    "\n",
    "The computation of attention starts with the multiplication of $Q$ and $K$ and the scaling of the result. \n"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "8bcdd30c472d081e"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[ 0.1212,  0.0520,  0.0058, -0.0115,  0.0520,  0.1617],\n         [ 0.0520,  0.2021,  0.0866,  0.1328,  0.1270, -0.0173],\n         [ 0.0058,  0.0866,  0.2829, -0.0577,  0.1790, -0.0577],\n         [-0.0115,  0.1328, -0.0577,  0.1674,  0.0115, -0.0693],\n         [ 0.0520,  0.1270,  0.1790,  0.0115,  0.1501,  0.0058],\n         [ 0.1617, -0.0173, -0.0577, -0.0693,  0.0058,  0.2598]]])"
     },
     "execution_count": 115,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "attention = torch.bmm(query, key.transpose(1, 2))\n",
    "attention = attention / torch.sqrt(torch.Tensor([3]))\n",
    "attention"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:40:01.998347650Z",
     "start_time": "2024-04-09T15:40:01.974640543Z"
    }
   },
   "id": "28861ba073bb8823",
   "execution_count": 115
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 2: softmax activation and computation of the scores\n",
    "\n",
    "The output of the previous multiplication is passed to the softmax function"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "5fdac14755e943c8"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[0.1762, 0.1644, 0.1570, 0.1543, 0.1644, 0.1835],\n         [0.1589, 0.1847, 0.1645, 0.1723, 0.1713, 0.1483],\n         [0.1546, 0.1676, 0.2039, 0.1451, 0.1838, 0.1451],\n         [0.1594, 0.1842, 0.1522, 0.1906, 0.1631, 0.1505],\n         [0.1605, 0.1730, 0.1822, 0.1541, 0.1770, 0.1532],\n         [0.1855, 0.1551, 0.1489, 0.1472, 0.1587, 0.2046]]])"
     },
     "execution_count": 116,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "probs = torch.softmax(attention, dim=-1)\n",
    "probs"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:40:03.790903931Z",
     "start_time": "2024-04-09T15:40:03.787140362Z"
    }
   },
   "id": "cb6a8db32cc17a89",
   "execution_count": 116
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 3: updating of query\n"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "8a9eaed3d226bd52"
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[0.2021, 0.2275, 0.1494],\n         [0.1955, 0.2118, 0.1868],\n         [0.2290, 0.1912, 0.1847],\n         [0.1798, 0.2182, 0.1868],\n         [0.2139, 0.2048, 0.1786],\n         [0.2022, 0.2385, 0.1283]]])"
     },
     "execution_count": 117,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "context = torch.bmm(probs, value)\n",
    "context"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:40:05.792260970Z",
     "start_time": "2024-04-09T15:40:05.761306913Z"
    }
   },
   "id": "cd89d07c18ef5774",
   "execution_count": 117
  },
  {
   "cell_type": "markdown",
   "source": [],
   "metadata": {
    "collapsed": false
   },
   "id": "642fea5fa76c09b6"
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 0: projection layers\n",
    "Usually, $Q$, $K$ and $V$ are multiplicated by matrices of learnable parameters $W^Q$, $W^K$ and $W^V$ before computing the attention. Namely, this is equivalent to implement a single linear layer with the same input size and output size $d$ (that is the dimensionality of the embedding).\n",
    "Also, the output is passed into a projection layer to get the updated embeddings"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "7ca92a4a1ed722be"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "d = 3\n",
    "\n",
    "query_projection = nn.Linear(d, d)\n",
    "key_projection = nn.Linear(d, d)\n",
    "value_projection = nn.Linear(d, d)\n",
    "out_projection = nn.Linear(d, d)\n",
    "\n",
    "query = query_projection(query)\n",
    "key = key_projection(key)\n",
    "value = value_projection(value)\n",
    "out = out_projection(context)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:40:07.452305830Z",
     "start_time": "2024-04-09T15:40:07.447083762Z"
    }
   },
   "id": "aeff57502cc269ea",
   "execution_count": 118
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Multi-Head Self Attention"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "526e2bd00855b3b9"
  },
  {
   "cell_type": "markdown",
   "source": [
    "While powerful, single-head self-attention has its limitations. It may struggle to capture diverse patterns and relationships in complex sequences. Imagine trying to understand a sentence containing multiple ideas, nuances, and dependencies with a single lens—it's challenging. Single-head self-attention operates similarly; it may not adequately capture the intricacies of language and context in such scenarios.\n",
    "\n",
    "This is where multi-head self-attention comes into play. Multi-head self-attention addresses the limitations of single-head self-attention by allowing the model to attend to different parts of the input simultaneously. By leveraging multiple attention mechanisms operating in parallel, multi-head self-attention enables the model to capture a broader range of patterns and dependencies in the data."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "ce25e8f0d7c2c0c4"
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Step 1: splitting heads\n",
    "\n",
    "After the projection layers, in MHSA we split the transformed key, value, and query matrices into multiple attention heads. This step involves reshaping the output of the linear transformations to separate the heads."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "9768f6b517d73386"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn\n",
    "\n",
    "input_embeds = torch.Tensor([[\n",
    "    [.2, .4, -.1, .1],\n",
    "    [.1, .3, .5, .2],\n",
    "    [.6, -.2, .3, -.1],\n",
    "    [-.3, .2, .4, .5],\n",
    "    [.4, .1, .3, .1],\n",
    "    [.2, .5, -.4, .2]\n",
    "]])\n",
    "query = input_embeds\n",
    "key = input_embeds\n",
    "value = input_embeds\n",
    "\n",
    "d = 4\n",
    "\n",
    "query_projection = nn.Linear(d, d)\n",
    "key_projection = nn.Linear(d, d)\n",
    "value_projection = nn.Linear(d, d)\n",
    "out_projection = nn.Linear(d, d)\n",
    "\n",
    "query = query_projection(query)\n",
    "key = key_projection(key)\n",
    "value = value_projection(value)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:42:10.667876262Z",
     "start_time": "2024-04-09T15:42:10.626966512Z"
    }
   },
   "id": "83729dc99288f176",
   "execution_count": 123
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[-0.4434,  0.1212,  0.0835, -0.3232],\n         [-0.4319, -0.0767, -0.0610, -0.1174],\n         [ 0.0653, -0.1274,  0.2230,  0.1377],\n         [-0.6655,  0.1014,  0.0199, -0.3536],\n         [-0.2099, -0.0865,  0.1373, -0.0288],\n         [-0.5438,  0.2279,  0.1939, -0.4816]]], grad_fn=<ViewBackward0>)"
     },
     "execution_count": 124,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "query"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:42:13.800131065Z",
     "start_time": "2024-04-09T15:42:13.784852287Z"
    }
   },
   "id": "9687232d36d0778e",
   "execution_count": 124
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "B, L, _ = query.shape\n",
    "H = 2 # n of heads \n",
    "assert d%H == 0 # H must be divisor of d, i.e. d%H must be 0\n",
    "\n",
    "query = query.view(B, L, H, -1)\n",
    "key = key.view(B, L, H, -1)\n",
    "value = value.view(B, L, H, -1)\n"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:42:15.158010650Z",
     "start_time": "2024-04-09T15:42:15.146873463Z"
    }
   },
   "id": "66e5c235d25c4194",
   "execution_count": 125
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[[-0.4434,  0.1212],\n          [ 0.0835, -0.3232]],\n\n         [[-0.4319, -0.0767],\n          [-0.0610, -0.1174]],\n\n         [[ 0.0653, -0.1274],\n          [ 0.2230,  0.1377]],\n\n         [[-0.6655,  0.1014],\n          [ 0.0199, -0.3536]],\n\n         [[-0.2099, -0.0865],\n          [ 0.1373, -0.0288]],\n\n         [[-0.5438,  0.2279],\n          [ 0.1939, -0.4816]]]], grad_fn=<ViewBackward0>)"
     },
     "execution_count": 126,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "query"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:42:17.101509938Z",
     "start_time": "2024-04-09T15:42:17.095302796Z"
    }
   },
   "id": "c3205dd5e20e7b70",
   "execution_count": 126
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 2: computation of attention\n",
    "\n",
    "We now compute the attention separately for each head"
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "d6377e666a429556"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "B, L, H, E = query.shape\n",
    "scale = 1. / torch.sqrt(torch.tensor([E]))"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T15:42:50.731979666Z",
     "start_time": "2024-04-09T15:42:50.675540464Z"
    }
   },
   "id": "58ea5a0f309a4b8d",
   "execution_count": 129
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "attention = torch.matmul(query, key.transpose(-2, -1))"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T16:01:22.479723706Z",
     "start_time": "2024-04-09T16:01:22.435997739Z"
    }
   },
   "id": "292ad06ee2df61a",
   "execution_count": 149
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "probs = torch.softmax(attention, dim=-1)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T16:02:52.235800810Z",
     "start_time": "2024-04-09T16:02:52.189694789Z"
    }
   },
   "id": "3235e362cff4e258",
   "execution_count": 152
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "context_vectors = torch.matmul(probs, value)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T16:02:57.286196213Z",
     "start_time": "2024-04-09T16:02:57.282820388Z"
    }
   },
   "id": "2c7a31fb618d6373",
   "execution_count": 153
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Step 3: concatenation and linear projection\n",
    "Finally, we concatenate the context vectors from all attention heads and apply a linear transformation to obtain the final multi-head self-attention output."
   ],
   "metadata": {
    "collapsed": false
   },
   "id": "740d99e4bffcf048"
  },
  {
   "cell_type": "code",
   "outputs": [],
   "source": [
    "context_vectors = context_vectors.permute(0, 2, 1, 3).contiguous().view(B, L, -1)\n",
    "output = out_projection(context_vectors)"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T16:04:37.547226110Z",
     "start_time": "2024-04-09T16:04:37.513318638Z"
    }
   },
   "id": "e6f053d47db124d4",
   "execution_count": 155
  },
  {
   "cell_type": "code",
   "outputs": [
    {
     "data": {
      "text/plain": "tensor([[[0.4117, 0.0733, 0.3038, 0.1904],\n         [0.2951, 0.1199, 0.2417, 0.1080],\n         [0.3325, 0.1236, 0.2804, 0.1561],\n         [0.4151, 0.0721, 0.2980, 0.1990],\n         [0.3042, 0.1093, 0.2423, 0.1056],\n         [0.3280, 0.1264, 0.2816, 0.1511]]], grad_fn=<ViewBackward0>)"
     },
     "execution_count": 156,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "output"
   ],
   "metadata": {
    "collapsed": false,
    "ExecuteTime": {
     "end_time": "2024-04-09T16:04:41.356046436Z",
     "start_time": "2024-04-09T16:04:41.314088004Z"
    }
   },
   "id": "91976d755b7a4101",
   "execution_count": 156
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
